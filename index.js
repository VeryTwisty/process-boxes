/**
 * External dependencies
 */
import { times } from 'lodash';
import classnames from 'classnames';
import memoize from 'memize';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { PanelBody, RangeControl, G, SVG, Path } = wp.components;
const { Fragment } = wp.element;
const {
	InspectorControls,
	InnerBlocks,
	ColorPalette
} = wp.editor;
const{
	dispatch
} = wp.data

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'core/column'.
 *
 * @constant
 * @type {string[]}
*/
const ALLOWED_BLOCKS = [ 'bbheritage/innerboxlist' ];


/**
 * Returns the layouts configuration for a given number of columns.
 *
 * @param {number} columns Number of columns.
 *
 * @return {Object[]} Columns layout configuration.
 */
const getColumnsTemplate = memoize( ( columns ) => {
	return times( columns, () => [ 'bbheritage/innerboxlist'] );
} );


// export const name = 'bbheritage/process-boxes';

export default registerBlockType(
	'bbheritage/processboxlist',
	{
		title: __( 'Process Boxes List' ),
		description: __( 'Add a block that displays content in multiple boxes' ),
		category: 'bb-heritage',
		icon: 'forms',
		keywords: [
			__( 'Staff', '_vt' ),
			__( 'BB Heritage Staff', '_vt' ),

		],

		attributes: {
			columns: {
				type: 'number',
				default: 3,
			},
			color: {
            	type: 'string',
            }
		},

		edit: props => {
			const { className, isSelected, setAttributes, clientId, attributes: { columns, color } } = props;

			const classes = classnames( className, `has-${ columns }-columns` );
			const innerListBlocks = wp.data.select('core/editor').getBlocksByClientId(clientId)[0].innerBlocks;
			

			innerListBlocks.map( block =>{
				console.log(block.clientId);
				dispatch('core/editor').updateBlockAttributes( block.clientId, { background: color } );
			});

			return (
				<React.Fragment>
					<InspectorControls>
						<PanelBody>
							<RangeControl
								label={ __( 'Columns' ) }
								value={ columns }
								onChange={ ( nextColumns ) => {
									setAttributes( {
										columns: nextColumns,
									} );
								} }
								min={ 2 }
								max={ 9 }
							/>
							<hr />
							<ColorPalette
		                        value={ color }
		                        onChange={ color => setAttributes( { color } ) }
		                    />
	                    
						</PanelBody>
					</InspectorControls>
					<div className="process-boxes">
						<h2>Process Boxes</h2>
						<div className={ classes }>
							<InnerBlocks
								template={ getColumnsTemplate( columns ) }
								templateLock="all"
								allowedBlocks={ ALLOWED_BLOCKS } />
						</div>
					</div>
				</React.Fragment>
			);
		},

		save( { attributes, className } ) {
			const { columns, color } = attributes;
			const classes = classnames( className, `has-${ columns }-columns` );

			return (
				
					<InnerBlocks.Content />
				
			);
		},
	},
);