/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { 
	RichText, 
	MediaUpload, 
	InspectorControls,
	PanelBody,
  	PanelRow,
  	URLInput,
  	ColorPalette
 } = wp.editor;
const {
    Button
} = wp.components;


export default registerBlockType(
	'bbheritage/innerboxlist',
	{
		title: __( 'Inner Box List' ),
		description: __( 'Child Block' ),
		parent: [ 'bbheritage/processboxlist' ],
		category: 'bb-heritage',
		icon: 'forms',

		attributes: {
			heading: {
				type: 'string',
			},
			description: {
				type: 'string',
			},
			imgURL: {
                type: 'string',
            },
            imgURLMob: {
                type: 'string',
            },
            imgID: {
                type: 'number',
            },
            url: {
            	type: 'string',
            },
            iconURL: {
                type: 'string',
            },
            iconID: {
                type: 'number',
            },
            iconUrl: {
            	type: 'string',
            },
            background:{
            	type: 'string',
            }
		},

		edit: props => {
			const { className, isSelected, setAttributes, attributes: { heading, description, imgURL, imgID, url, imgURLMob, iconURL, iconID, iconUrl, background } } = props;

			console.log(props.attributes)

			const onSelectImage = img => { 

				let url = img.sizes.thumbnail ? img.sizes.thumbnail.url : img.sizes.full.url;
				let urlMob = img.sizes.medium ? img.sizes.medium.url : img.sizes.full.url;

				setAttributes({
					imgID: img.id,
	                imgURL: url,
	                imgURLMob: urlMob,
	            });
			};
			const onSelectIcon = img => { 

				let url = img.sizes.thumbnail ? img.sizes.thumbnail.url : img.sizes.full.url;

				setAttributes({
					iconID: img.id,
	                iconURL: url,
	            });
			};

			return [
					<InspectorControls>
						<h2>Pannel Text</h2>
						<Fragment>
						<RichText
							tagName="h3"
							placeholder={ __( "Box title", "_vt" ) }
							onChange= { heading => { setAttributes( { heading } ) } }
							value={ heading }
						/>
						<RichText
							tagName="div"
							placeholder={ __( "Box description", "_vt" ) }
							onChange= { description => { setAttributes( { description } ) } }
							value={ description }
							wrapperClassName=".box-content"
						/>
						<hr />
						<h3>Icon Image</h3>
						<MediaUpload
							onSelect={ onSelectImage }
							title="Upload Image"
							allowedTypes={ ['image'] }
							value={ iconID }
							render={ ( { open } ) => (
								! iconID ? (
									<Button onClick={ open } className={ "button button-large margin-center" } >
										Add Icon
									</Button>
								):(
									isSelected ? (
										<Fragment>
											<img class="iconImg" src={iconURL} width="100px" />
											<Button onClick={ open } className={ "button button-large margin-center" } >
												Change Icon
											</Button>
										</Fragment>
									):(null)
								)
							) } />
						<hr />
						<h3>Side Image</h3>
						<MediaUpload
							onSelect={ onSelectImage }
							title="Upload Image"
							allowedTypes={ ['image'] }
							value={ imgID }
							render={ ( { open } ) => (
								! imgID ? (
									<Button onClick={ open } className={ "button button-large margin-center" } >
										Add Image
									</Button>
								):(
									isSelected ? (
										<Fragment>
											<img class="iconImg" src={imgURL} width="100px" />
											<Button onClick={ open } className={ "button button-large margin-center" } >
												Change Image
											</Button>
										</Fragment>
									):(null)
								)
							) } />
							
						</Fragment>
						
					</InspectorControls>,

					<div className={className}>
						<div className="box-content">

							<div className="icon" style={ { background: background } }>
						
								{
									iconURL ? (
										<img class="iconImg" src={iconURL}  />
									) : ( null )
								}	

								<MediaUpload
									onSelect={ onSelectIcon }
									title="Upload Image"
									allowedTypes={ ['image'] }
									value={ iconID }
									render={ ( { open } ) => (
										! iconID ? (
											<Button onClick={ open } className={ "button button-large margin-center" } >
												Add Icon
											</Button>
										):(
											isSelected ? (
												<Button onClick={ open } className={ "button button-large margin-center" } >
													Change Icon
												</Button>
											):(null)
										)
									) } />
							</div>

							<RichText
								tagName="h3"
								placeholder={ __( "Box title", "_vt" ) }
								onChange= { heading => { setAttributes( { heading } ) } }
								value={ heading }
							/>


						

							<RichText
								tagName="div"
								placeholder={ __( "Box description", "_vt" ) }
								onChange= { description => { setAttributes( { description } ) } }
								value={ description }
								wrapperClassName="box-content"
							/>
						</div>

						<div className="image">
						{
							imgURL ? (
								<img src={imgURL} />
							) : ( null )
						}				

						<MediaUpload
							onSelect={ onSelectImage }
							title="Upload Image"
							allowedTypes={ ['image'] }
							value={ imgID }
							render={ ( { open } ) => (
							! imgID ? (
								<Button onClick={ open } className={ "button button-large margin-center" } >
									Add Image
								</Button>
							):(
								isSelected ? (
									<Button onClick={ open } className={ "button button-large margin-center" } >
										Change Image
									</Button>
								):(null)
							)
						) } />		
									
						</div>

						<hr />

					</div>
			];
		},

		save: props => {

			const { className, attributes: { heading, description, imgURL, imgURLMob, imgID, iconURL, iconID, iconUrl, background } } = props;

			return( 
				<div className={ className }>

					<div className="box-content">
						
						{ iconURL ? ( <div className="icon" style={ { background: background } }><img src={ iconURL } /></div> ):( null ) }
						<h3>{ heading }</h3>
						{ description ? ( <p>{description}</p> ):( null ) }				
					</div>
					{ imgURL ? ( 
						<div className="image">
							<picture>
							<source media="(min-width: 769px)" srcset={ imgURL } />
							<source media="(min-width: 200px)" srcset={ imgURLMob } />
							<img src={ imgURL } alt=" " />
							</picture>
						</div> 
						):( null ) }
					<hr />
				</div>
			);

		},
		deprecated: [
	        {
	            save: props => {

					const { className, attributes: { heading, description, imgURL, imgURLMob, imgID, iconURL, iconID, iconUrl } } = props;

					return( 
						<div className={ className }>

					<div className="box-content">
						
						{ iconURL ? ( <div className="icon"><img src={ iconURL } /></div> ):( null ) }
						<h3>{ heading }</h3>
						{ description ? ( <p>{description}</p> ):( null ) }				
					</div>
					{ imgURL ? ( 
						<div className="image">
							<picture>
							<source media="(min-width: 769px)" srcset={ imgURL } />
							<source media="(min-width: 200px)" srcset={ imgURLMob } />
							<img src={ imgURL } alt=" " />
							</picture>
						</div> 
						):( null ) }
					<hr />
				</div>
					);

				},

	        }
	    ]
	},
);